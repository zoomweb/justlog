module github.com/gempir/justlog

go 1.16

require (
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/nicklaw5/helix v1.0.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)
